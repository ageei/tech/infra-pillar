
# WARNING
#
# We don't manage regular users sudo entries here.
# Use `pillar/users.sls` or the `users` formula instead.
#

#
# To push iptables hit counters, telegraf must be able to call iptables.
# The below rule (1) allows it to call `sudo iptables -nvL*` without
# passwords and (2) prevents the sudo logs from being sent to the syslog
# server on invocation by settings defaults for the IPTABLESHOW command
# alias.
#
sudoers:
  manage_main_config: True

  groups:
    sudo:
      - 'ALL=(ALL) NOPASSWD: ALL'

  aliases:
    commands:
      IPTABLESHOW:
        - '/sbin/iptables -nvL'
  defaults:
    command_list:
      IPTABLESHOW: '!logfile, !pam_session, !syslog'
  users:
    telegraf:
      - 'ALL=(root) NOPASSWD: IPTABLESHOW'
