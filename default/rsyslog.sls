#
# Allow local processes on udp://127.0.0.1:514
# Relay messages through the tunnel at relp://elk.inf:1514
#

rsyslog:
  config: |
    module(load="imudp")
    input(type="imudp" port="514")
    module(load="omrelp")
    action(type="omrelp" target="169.254.0.3" port="1514")
