#!jinja|yaml

{% import "env.jinja" as env %}
{% if 'final' == env.stage %}
  {% set upstream = 'http://apt.inf' %}
{% else %}
  {% set upstream = 'https://mirrors.kernel.org' %}
{% endif %}


apt:
  archives:
    - deb {{ upstream }}/ubuntu bionic main restricted universe multiverse
    - deb {{ upstream }}/ubuntu bionic-updates main restricted universe multiverse
    - deb {{ upstream }}/ubuntu bionic-security main restricted universe multiverse

  local: []
  common:
    - saltstack
    - telegraf

  vendors:
    docker:
      keyid: '0x9DC858229FC7DD38854AE2D88D81803C0EBFCD88'
      source: |
{%- if 'final' == env.stage %}
        deb http://apt.inf/docker/ubuntu bionic stable
{%- else %}
        deb https://download.docker.com/linux/ubuntu bionic stable
{% endif %}

    elastic:
      keyid: '0x46095ACC8548582C1A2699A9D27D666CD88E42B4'
      source: |
{%- if 'final' == env.stage %}
        deb http://apt.inf/elastic/7.x/apt stable main
{%- else %}
        deb https://artifacts.elastic.co/packages/7.x/apt stable main
{% endif %}

    gitlab-runner:
      keyid: '0x1A4C919DB987D435939638B914219A96E15E78F4'
      source: |
{%- if 'final' == env.stage %}
        deb http://apt.inf/gitlab-runner/ubuntu bionic main
{%- else %}
        deb https://packages.gitlab.com/runner/gitlab-runner/ubuntu bionic main
{% endif %}

    grafana:
      keyid: '0x4E40DDF6D76E284A4A6780E48C8C34C524098CB6'
      source: |
{%- if 'final' == env.stage %}
        deb http://apt.inf/grafana/deb stable main
{%- else %}
        deb https://packages.grafana.com/oss/deb stable main
{% endif %}

    saltstack:
      keyid: '0x754A1A7AE731F165D5E6D4BD0E08A149DE57BFBE'
      source: |
{%- if 'final' == env.stage %}
        deb http://apt.inf/saltstack/apt/ubuntu/18.04/amd64/2019.2 bionic main
{%- else %}
        deb https://repo.saltstack.com/apt/ubuntu/18.04/amd64/2019.2 bionic main
{% endif %}

    telegraf:
      keyid: '0x05ce15085fc09d18e99efb22684a14cf2582e0c5'
      source: |
{%- if 'final' == env.stage %}
        deb http://apt.inf/telegraf/ubuntu bionic stable
{%- else %}
        deb https://repos.influxdata.com/ubuntu bionic stable
{% endif %}
