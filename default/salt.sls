#!jinja|yaml

{% import "env.jinja" as env %}

# We keep the master configuration separate for simplicity.
{% macro master_cfg() %}{% include "default/salt-master.yml" %}{% endmacro %}


salt:
  clean_config_d_dir: False
  # We want to keep our salt-masterless remotes until
  # we are ready to apply the final configuration.
  minion_remove_config: {{ 'final' == env.stage }}
  master_remove_config: True
  py_ver: 'py3'
  install_packages: True
  lookup:
    pyinotify: 'python3-pyinotify'

  master:
    {{ master_cfg() | indent(4) }}

{% if 'final' == env.stage %}
  minion:
    master: salt.inf
    grains:
      env: {{ env.name }}
{% endif %}
