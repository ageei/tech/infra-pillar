#!jinja|yaml

include:
  - default.apt
  - default.rsyslog
  - default.salt
  - default.sudo
  - default.telegraf
