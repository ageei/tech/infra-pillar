#!jinja|yaml

{% import "env.jinja" as env %}


dhparam:
  nginx: /etc/ssl/nginx.dhparam


nginx:
  install_from_repo: False
  install_from_phusionpassenger: False
  install_from_ppa: False

  lookup:
    package: nginx-light

  service:
    enable: True

  servers:
    managed:
      default:
        enabled: True
        config:
          - server:
            - error_log: 'syslog:server=127.0.0.1'
            - access_log: 'syslog:server=127.0.0.1'
            - server_name: '_'
            - listen:
              - '80 default_server'
            - 'location = /status':
              # WARNING This is a hack. nginx ignores the 1 and
              # it keeps nginx-formula from putting stub_status on the
              # same line as the location -\_(O.O)_/-
              - stub_status: 1
              - allow: 127.0.0.1
              - deny: all
            - 'location /.well-known/acme-challenge/':
              - proxy_pass: 'http://127.0.0.1:8080/.well-known/acme-challenge/'
              - proxy_redirect: "off"
              - proxy_set_header: 'X-Real-IP $remote_addr'
            - 'location /':
              - return: '301 https://$host$request_uri'
          - server:
            - error_log: 'syslog:server=127.0.0.1'
            - access_log: 'syslog:server=127.0.0.1'
            - server_name: '_'
            - listen:
              - '443 default_server ssl'
            - ssl_certificate: '/etc/letsencrypt/live/wiki.{{ env.domains[0] }}/fullchain.pem'
            - ssl_certificate_key: '/etc/letsencrypt/live/wiki.{{ env.domains[0] }}/privkey.pem'
            - ssl_session_timeout: '1d'
            - ssl_session_cache: 'shared:AGEEI:10m'
            - ssl_session_tickets: 'off'
            - ssl_dhparam: '/etc/ssl/nginx.dhparam'
            - ssl_protocols: 'TLSv1.2 TLSv1.3'
            - ssl_ciphers: 'ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384'
            - ssl_prefer_server_ciphers: 'on'
            - add_header: 'Strict-Transport-Security "max-age=63072000" always'
            - 'location /_register':
              - return: 404
            - 'location /':
              - proxy_pass: 'http://127.0.0.1:5001'
              - proxy_set_header: 'X-Real-IP $remote_addr'
              - proxy_redirect: 'off'
