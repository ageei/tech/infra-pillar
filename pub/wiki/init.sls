#!jinja|yaml

{% import "env.jinja" as env %}

include:
  - pub.wiki.gitit
  - pub.wiki.nginx
  - pub.wiki.rc-local
  - pub.wiki.rules
  - pub.wiki.telegraf

snakeoil:
  wiki.{{ env.domains[0] }}: []
