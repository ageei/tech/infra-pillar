#!jinja|yaml

{% import "env.jinja" as env %}


haproxy:
  enabled: True
  global:
    user: root
    stats:
      enable: True
      socketpath: /var/lib/haproxy/stats
      mode: 660
      level: admin
    chroot:
      enable: True
      path: /var/lib/haproxy
    daemon: True

  defaults:
    log: global
    mode: http
    options:
      - httplog
      - dontlognull
      - http-server-close
      - forwardfor except 127.0.0.1
      - redispatch
    retries: 3
    maxconn: 3000
    timeouts:
      - http-request	10s
      - queue		1m
      - connect		10s
      - client		1m
      - server		1m
      - http-keep-alive	10s
      - check		10s

  frontends:
    ft_ssh:
      bind: '*:22'
      mode: tcp
      use_backends:
        - bk_ctf_pwn_ssh
    ft_http:
      bind: '*:80 transparent'
      mode: http
      acls:
        - 'uqam_ca  hdr(host) -i ageei.uqam.ca'
        - 'uqam_ca  hdr(host) -m end .ageei.uqam.ca'
        - 'pub_wiki hdr(host) -i wiki.{{ env.domains[0] }}'
        - 'ctf_ctfd hdr(host) -i ctf.{{ env.domains[0] }}'
        - 'ctf_web  hdr(host) -m end .web.ctf.{{ env.domains[0] }}'
      use_backends:
        - bk_transit_http  if uqam_ca
        - bk_pub_wiki_http if pub_wiki
        - bk_ctf_ctfd_http if ctf_ctfd
        - bk_ctf_web_http  if ctf_web
    ft_https:
      bind: '*:443 transparent'
      mode: tcp
      extra:
        - tcp-request inspect-delay 5s
        - 'tcp-request content accept if { req_ssl_hello_type 1 }'
      acls:
        - 'ssh          payload(0,7) -m bin 5353482d322e30'
        - 'transit	req_ssl_sni -i ageei.uqam.ca'
        - 'transit	req_ssl_sni -m end .ageei.uqam.ca'
        - 'pub_wiki	req_ssl_sni -i wiki.{{ env.domains[0] }}'
        - 'ctf_ctfd	req_ssl_sni -i ctf.{{ env.domains[0] }}'
        - 'ctf_web	req_ssl_sni -m end .web.ctf.{{ env.domains[0] }}'
      use_backends:
        - bk_pub_wiki_https	if pub_wiki
        - bk_ctf_ctfd_https	if ctf_ctfd
        - bk_ctf_web_https	if ctf_web
        - bk_transit_https	if transit
        - bk_ssh		if ssh
    # Waiting for a nice .onion before activating
    #ft_onion:
    #  bind: '127.0.0.1:8000'
    #  mode: http
    #  # Since traffic is not encrypted at this point, we can only
    #  # safely serve things that do not contain private or identifiable
    #  # information: !logins (e.g. ctfd).
    #  acls:
    #    - 'pub_wiki_onion	hdr(host) -i wiki.FIXME'
    #    - 'ctf_web_onion       hdr(host) -i web.ctf.FIXME'
    #  use_backends:
    #    - bk_pub_wiki_http	if pub_wiki_onion
    #    - bk_ctf_web_http	if ctf_web_onion

  backends:
    bk_ssh:
      mode: tcp
      servers:
        ssh:
          host: 127.0.0.1
          port: 2222
    bk_transit_http:
      mode: http
      servers:
        transit-http:
          host: 127.0.0.1
          port: 8080
    bk_transit_https:
      mode: tcp
      option:
        - ssl-hello-chk
      servers:
        transit-https:
          host: 127.0.0.1
          port: 8443
    bk_pub_wiki_http:
      mode: http
      extra:
        - source 0.0.0.0 usesrc clientip
      servers:
        pub-wiki-http:
          host: 10.0.2.2
          port: 80
    bk_pub_wiki_https:
      mode: tcp
      options:
        - ssl-hello-chk
      extra:
        - source 0.0.0.0 usesrc clientip
      servers:
        pub-wiki-https:
          host: 10.0.2.2
          port: 443
    bk_ctf_ctfd_http:
      mode: http
      extra:
        - source 0.0.0.0 usesrc clientip
      servers:
        ctf-ctfd-http:
          host: 10.0.3.2
          port: 80
    bk_ctf_ctfd_https:
      mode: tcp
      options:
        - ssl-hello-chk
      extra:
        - source 0.0.0.0 usesrc clientip
      servers:
        ctf-ctfd-https:
          host: 10.0.3.2
          port: 443
    bk_ctf_web_http:
      mode: http
      extra:
        - source 0.0.0.0 usesrc clientip
      servers:
        ctf-web-http:
          host: 10.0.3.3
          port: 80
    bk_ctf_web_https:
      mode: tcp
      options:
        - ssl-hello-chk
      extra:
        - source 0.0.0.0 usesrc clientip
      servers:
        ctf-web-https:
          host: 10.0.3.3
          port: 443
    bk_ctf_pwn_ssh:
      mode: tcp
      extra:
        - source 0.0.0.0 usesrc clientip
      servers:
        ctf-pwn-ssh:
          host: 10.0.3.4
          port: 22
