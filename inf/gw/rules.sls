#!jinja|yaml

{% import "env.jinja" as env %}


rules.v4: |
{%- if 'final' == env.stage %}
 *filter
 :INPUT   DROP [0:0]
 :FORWARD DROP [0:0]
 :OUTPUT  DROP [0:0]

 # First rule so we catch everything, even on loopback.
 # Log DNS queries not from the Internet.
 -A INPUT  ! -i eth0 -p udp --dport 5353 -j NFLOG --nflog-group 53
 -A INPUT  ! -i eth0 -p tcp --dport 5353 -j NFLOG --nflog-group 53
 # Log DNS replies
 -A OUTPUT -p udp --sport 5353 -j NFLOG --nflog-group 53
 -A OUTPUT -p tcp --sport 5353 -j NFLOG --nflog-group 53
 # Do the actual DNS interception
 # FIXME Allows direct traffic to port 5353.
 -A INPUT  ! -i eth0 -p udp --dport 5353 -j ACCEPT
 -A INPUT  ! -i eth0 -p tcp --dport 5353 -j ACCEPT
 -A OUTPUT -o eth0 -d 127.0.0.1 -p udp --dport 5353 -j ACCEPT
 -A OUTPUT -o eth0 -d 127.0.0.1 -p tcp --dport 5353 -j ACCEPT

 # Let everything on loopback fly
 -A INPUT  -i lo -j ACCEPT
 -A OUTPUT -o lo -j ACCEPT

 # Allow established
 -A INPUT  -m state --state ESTABLISHED,RELATED -j ACCEPT
 -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

 # ingress traffic
 -A INPUT  -i eth0 -p tcp --dport 22  -j ACCEPT -m comment --comment "incoming ssh"
 -A INPUT  -i eth0 -p tcp --dport 80  -j ACCEPT -m comment --comment "incoming http"
 -A INPUT  -i eth0 -p tcp --dport 443 -j ACCEPT -m comment --comment "incoming https"

 # egress traffic
 -A OUTPUT -o eth0    -p tcp --dport 443       -d 9.9.9.9/32     -j ACCEPT -m comment --comment "outgoing dns"
 -A OUTPUT -o eth-inf -p tcp --dport 80        -d 169.254.0.2/32 -j ACCEPT -m comment --comment "outgoing pkg install"
 -A OUTPUT -o eth-inf -p tcp --dport 1514      -d 169.254.0.3/32 -j ACCEPT -m comment --comment "outgoing syslog"
 -A OUTPUT -o eth-inf -p tcp --dport 8086      -d 169.254.0.4/32 -j ACCEPT -m comment --comment "outgoing metrics"
 -A OUTPUT -o eth-inf -p tcp --dport 4505:4506 -d 169.254.0.5/32 -j ACCEPT -m comment --comment "outgoing salt"
 -A OUTPUT -o eth-pub -p tcp --dport 80        -d 10.0.2.2/32    -j ACCEPT -m comment --comment "outgoing to pub.wiki:80"
 -A OUTPUT -o eth-pub -p tcp --dport 443       -d 10.0.2.2/32    -j ACCEPT -m comment --comment "outgoing to pub.wiki:443"
 -A OUTPUT -o eth-ctf -p tcp --dport 80        -d 10.0.3.2/32    -j ACCEPT -m comment --comment "outgoing to ctfd.ctf:80"
 -A OUTPUT -o eth-ctf -p tcp --dport 443       -d 10.0.3.2/32    -j ACCEPT -m comment --comment "outgoing to ctfd.ctf:443"
 -A OUTPUT -o eth-ctf -p tcp --dport 80        -d 10.0.3.3/32    -j ACCEPT -m comment --comment "outgoing to web.ctf:80"
 -A OUTPUT -o eth-ctf -p tcp --dport 22        -d 10.0.3.4/32    -j ACCEPT -m comment --comment "outgoing to pwn.ctf:22"

 # DHCP
 -A INPUT ! -i eth0 -p udp --dport 67 -j ACCEPT -m comment --comment "incoming DHCP requests"
 -A OUTPUT          -p udp --dport 68 -j ACCEPT -m comment --comment "outgoing DHCP replies"

 -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
 -A FORWARD -i eth0 -j DROP

 -A FORWARD -i eth-inf -o eth0 -s 169.254.0.2/32 -p tcp --dport 443 -j ACCEPT -m comment --comment "apt.inf fetching packages"
 -A FORWARD -i eth-inf -o eth0 -s 169.254.0.5/32 -p tcp --dport 443 -j ACCEPT -m comment --comment "salt.inf fetching configs"

 # from any subnet to .inf services
 -A FORWARD -o eth-inf -d 169.254.0.2/32 -p tcp --dport 80        -j ACCEPT -m comment --comment "hosts installing packages"
 -A FORWARD -o eth-inf -d 169.254.0.3/32 -p tcp --dport 1514      -j ACCEPT -m comment --comment "hosts syslog"
 -A FORWARD -o eth-inf -d 169.254.0.4/32 -p tcp --dport 8086      -j ACCEPT -m comment --comment "hosts metrics"
 -A FORWARD -o eth-inf -d 169.254.0.5/32 -p tcp --dport 4505:4506 -j ACCEPT -m comment --comment "hosts fetching configs"

 -A FORWARD -i eth-dev -s 10.0.1.2/32 -o eth0    -p tcp --dport 443  -j ACCEPT -m comment --comment "lxd.dev egress 443"
 -A FORWARD -i eth-pub -s 10.0.2.2/32 -o eth0    -p tcp --dport 443  -j ACCEPT -m comment --comment "wiki.pub egress 443"
 -A FORWARD -i eth-ctf -s 10.0.3.2/32 -o eth0    -p tcp --dport 443  -j ACCEPT -m comment --comment "ctfd.ctf egress 443"
 -A FORWARD -i eth-ctf -s 10.0.3.3/32 -o eth0    -p tcp --dport 443  -j ACCEPT -m comment --comment "web.ctf egress 443"
 -A FORWARD -i eth-ctf -s 10.0.3.4/32 -o eth0    -p tcp --dport 443  -j ACCEPT -m comment --comment "pwn.ctf egress 443"
 # TODO finalize policies

 -A INPUT   -j NFLOG
 -A FORWARD -j NFLOG
 -A OUTPUT  -j NFLOG
 COMMIT

 *nat
 :PREROUTING  ACCEPT [0:0]
 :INPUT       ACCEPT [0:0]
 :POSTROUTING ACCEPT [0:0]
 :OUTPUT      ACCEPT [0:0]

 # transparently proxy all DNS traffic to the running unbound instance
 # the unbound process may use port 53
 -A PREROUTING -p tcp --dport 53 -j REDIRECT --to-port 5353
 -A PREROUTING -p udp --dport 53 -j REDIRECT --to-port 5353
 -A OUTPUT     -p tcp --dport 53 -m owner ! --uid-owner unbound -j REDIRECT --to-port 5353
 -A OUTPUT     -p udp --dport 53 -m owner ! --uid-owner unbound -j REDIRECT --to-port 5353

 -A POSTROUTING -o eth0 -j MASQUERADE
 COMMIT

 # Transparent proxying
 *mangle
 -N DIVERT
 -A PREROUTING -p tcp -m socket -j DIVERT
 -A DIVERT -j MARK --set-mark 1
 -A DIVERT -j ACCEPT
 COMMIT
{% endif %}
