#!jinja|yaml
#
# TODO in prod, eth0 has a static ip
#

ifupdown:
{%- for nic in ['lo', 'eth0', 'eth-inf', 'eth-srv', 'eth-dev', 'eth-pub', 'eth-ctf'] %}
  - stanza: auto
    target: {{ nic }}
{% endfor %}
  - stanza: iface
    target: lo
    family: inet
    method: loopback
  - stanza: iface
    target: eth0
    family: inet
    method: dhcp
  - stanza: iface
    target: eth-inf
    family: inet
    method: static
    contents:
      - address 169.254.0.1
      - netmask 255.255.255.0
      - broadcast-address 169.254.0.255
  - stanza: iface
    target: eth-srv
    family: inet
    method: static
    contents:
      - address 10.0.0.1
      - netmask 255.255.255.0
      - broadcast-address 10.0.0.255
  - stanza: iface
    target: eth-dev
    family: inet
    method: static
    contents:
      - address 10.0.1.1
      - netmask 255.255.255.0
      - broadcast-address 10.0.1.255
  - stanza: iface
    target: eth-pub
    family: inet
    method: static
    contents:
      - address 10.0.2.1
      - netmask 255.255.255.0
      - broadcast-address 10.0.2.255
  - stanza: iface
    target: eth-ctf
    family: inet
    method: static
    contents:
      - address 10.0.3.1
      - netmask 255.255.255.0
      - broadcast-address 10.0.3.255
