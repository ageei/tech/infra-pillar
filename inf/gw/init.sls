
include:
  - inf.users
  - inf.gw.dnsmasq
  - inf.gw.haproxy
  - inf.gw.ifupdown
  - inf.gw.rc-local
  - inf.gw.rules
  - inf.gw.ssh
  - inf.gw.unbound
  #- inf.gw.sshproxy
