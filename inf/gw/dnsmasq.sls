#!jinja|yaml

{% import "env.jinja" as env %}


dnsmasq:
  dnsmasq_conf: salt://dnsmasq/files/dnsmasq.conf
  settings:
    port: 0
    bind-interfaces: True
    dhcp-authoritative: True
    dhcp-option: 6,9.9.9.9

    interface:
      - eth-ctf
      - eth-dev
      - eth-inf
      - eth-pub
      - eth-srv

    # We assign each address statically
    dhcp-range:
      - 169.254.0.0,static
      - 10.0.0.0,static
      - 10.0.1.0,static
      - 10.0.2.0,static
      - 10.0.3.0,static

    dhcp-host:
      # inf-apt
      - 00:16:3e:a9:fe:02,169.254.0.2
      # inf-elk
      - 00:16:3e:a9:fe:03,169.254.0.3
      # inf-tig
      - 00:16:3e:a9:fe:04,169.254.0.4
      # inf-salt
      - 00:16:3e:a9:fe:05,169.254.0.5
      # dev-lxd
      - 00:16:3e:00:01:02,10.0.1.2
      # pub-wiki
      - 00:16:3e:00:02:02,10.0.2.2
      # ctf-ctfd
      - 00:16:3e:00:03:02,10.0.3.2
      # ctf-web
      - 00:16:3e:00:03:03,10.0.3.3
      # ctf-pwn
      - 00:16:3e:00:03:04,10.0.3.4
