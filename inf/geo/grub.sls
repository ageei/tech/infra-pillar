
grub:
  config: |
    GRUB_CMDLINE_LINUX_DEFAULT="console=tty0 console=ttyS0 apparmor=1 security=apparmor"
    # wait for the SANs to be up
    GRUB_CMDLINE_LINUX="rootdelay=120"
    GRUB_TERMINAL=console
    GRUB_TIMEOUT=5
    unset GRUB_HIDDEN_TIMEOUT
    unset GRUB_HIDDEN_TIMEOUT_HIDDEN
    unset GRUB_TIMEOUT_STYLE
