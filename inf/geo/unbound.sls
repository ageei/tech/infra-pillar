# FIXME shares a lot with gw.inf

unbound:
  lookup:
    server:
      port: 5353
      hide-identity: "yes"
      hide-version: "yes"
      do-not-query-localhost: "no"
      access-control:
        - 0.0.0.0/0 allow
      interface-automatic: "yes"
      cache-max-negative-ttl: 1

      local-zone:
        - '"inf." static'
      local-data:
        - "apt.inf. IN A 169.254.0.2"
        - "elk.inf. IN A 169.254.0.3"
        - "tig.inf. IN A 169.254.0.4"
        - "salt.inf. IN A 169.254.0.5"
      local-data-ptr:
        - "169.254.0.2 apt.inf."
        - "169.254.0.3 elk.inf."
        - "169.254.0.4 tig.inf."
        - "169.254.0.5 salt.inf."

    remote-control:
      control-enable: "yes"
      control-interface: 127.0.0.1
      control-port: 8953
      control-use-cert: "no"

    forward-zone:
      ".":
        forward-addr:
          - 127.0.0.1@8053
