#!jinja|yaml

{% import "env.jinja" as env %}

{% macro p_default()   %}{% include "inf/geo/profiles/default.yml" %}{% endmacro %}
{% macro p_inf_gw()    %}{% include "inf/geo/profiles/inf-gw.yml" %}{% endmacro %}
{% macro p_inf_apt()   %}{% include "inf/geo/profiles/inf-apt.yml" %}{% endmacro %}
{% macro p_inf_elk()   %}{% include "inf/geo/profiles/inf-elk.yml" %}{% endmacro %}
{% macro p_inf_tig()   %}{% include "inf/geo/profiles/inf-tig.yml" %}{% endmacro %}
{% macro p_inf_salt()  %}{% include "inf/geo/profiles/inf-salt.yml" %}{% endmacro %}
{% macro p_dev_lxd()   %}{% include "inf/geo/profiles/dev-lxd.yml" %}{% endmacro %}
{% macro p_pub_wiki()  %}{% include "inf/geo/profiles/pub-wiki.yml" %}{% endmacro %}
{% macro p_ctf_ctfd()  %}{% include "inf/geo/profiles/ctf-ctfd.yml" %}{% endmacro %}
{% macro p_ctf_web()   %}{% include "inf/geo/profiles/ctf-web.yml" %}{% endmacro %}
{% macro p_ctf_pwn()   %}{% include "inf/geo/profiles/ctf-pwn.yml" %}{% endmacro %}


lxd.subids:
  base: 100000000
  size: 13000000


lxd:
  lxd:
    run_init: True
    init:
      storage_backend: zfs
      storage_pool: vpool/lxd

  profiles:
    local:
      default:
        {{ p_default() | indent(8) }}
{% if 'prd' != env.name %}
      net:
        devices:
          eth0:
            name: eth0
            type: nic
            nictype: bridged
            parent: lxdbr0
            hwaddr: 00:16:3e:12:34:56
            security.mac_filtering: "true"
{% endif %}
      inf-gw:
        {{ p_inf_gw() | indent(8) }}
      inf-apt:
        {{ p_inf_apt() | indent(8) }}
      inf-elk:
        {{ p_inf_elk() | indent(8) }}
      inf-tig:
        {{ p_inf_tig() | indent(8) }}
      inf-salt:
        {{ p_inf_salt() | indent(8) }}
      dev-lxd:
        {{ p_dev_lxd() | indent(8) }}
      pub-wiki:
        {{ p_pub_wiki() | indent(8) }}
      ctf-ctfd:
        {{ p_ctf_ctfd() | indent(8) }}
      ctf-web:
        {{ p_ctf_web() | indent(8) }}
      ctf-pwn:
        {{ p_ctf_pwn() | indent(8) }}
