#!jinja|yaml
# FIXME

{% import "env.jinja" as env %}


rules.v4: |
{%- if 'final' == env.stage %}
 *filter
 :INPUT   DROP [0:0]
 :FORWARD DROP [0:0]
 :OUTPUT  DROP [0:0]
 -A INPUT  -i lo -j ACCEPT
 -A OUTPUT -o lo -j ACCEPT
 -A INPUT  -m state --state ESTABLISHED,RELATED -j ACCEPT
 -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
 -A INPUT  -i eth0 -p tcp --dport 22  -j ACCEPT -m comment --comment "incoming ssh"
 # egress traffic
 -A OUTPUT -d 127.0.0.1 -p udp --dport 5353 -j ACCEPT -m comment --comment "dns queries"
 -A OUTPUT -o br-main -p tcp --dport 443       -d 9.9.9.9/32     -j ACCEPT -m comment --comment "outgoing dns"
 -A OUTPUT -o br-inf  -p tcp --dport 80        -d 169.254.0.2/32 -j ACCEPT -m comment --comment "outgoing pkg install"
 -A OUTPUT -o br-inf  -p tcp --dport 1514      -d 169.254.0.3/32 -j ACCEPT -m comment --comment "outgoing syslog"
 -A OUTPUT -o br-inf  -p tcp --dport 8086      -d 169.254.0.4/32 -j ACCEPT -m comment --comment "outgoing metrics"
 -A OUTPUT -o br-inf  -p tcp --dport 4505:4506 -d 169.254.0.5/32 -j ACCEPT -m comment --comment "outgoing salt"

{%- if 'prd' != env.name %}
 # DHCP (!prod only)
 -A INPUT ! -i eth0 -p udp --dport 67 -j ACCEPT -m comment --comment "incoming DHCP requests"
 -A OUTPUT          -p udp --dport 68 -j ACCEPT -m comment --comment "outgoing DHCP replies"
{%- endif %}

 -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
 -A FORWARD -i br-main -o {{ env.iface }} -p tcp ! --dport 25 -j ACCEPT
 -A FORWARD -i {{ env.iface }} -j DROP

 -A INPUT   -j NFLOG
 -A FORWARD -j NFLOG
 -A OUTPUT  -j NFLOG
 COMMIT

 *nat
 :PREROUTING  ACCEPT [0:0]
 :INPUT       ACCEPT [0:0]
 :POSTROUTING ACCEPT [0:0]
 :OUTPUT      ACCEPT [0:0]

 # transparently proxy all DNS traffic to the running unbound instance
 # the unbound process may use port 53
 -A OUTPUT     -p tcp --dport 53 -m owner ! --uid-owner unbound -j REDIRECT --to-port 5353
 -A OUTPUT     -p udp --dport 53 -m owner ! --uid-owner unbound -j REDIRECT --to-port 5353
 COMMIT
{% endif %}
