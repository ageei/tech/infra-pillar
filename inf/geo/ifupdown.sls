#!jinja|yaml

{% import "env.jinja" as env %}


ifupdown:
{%- for nic in ['lo', env.iface, 'br-main', 'br-inf', 'br-srv', 'br-dev', 'br-pub', 'br-ctf'] %}
  - stanza: auto
    target: {{ nic }}
{% endfor %}
  - stanza: iface
    target: lo
    family: inet
    method: loopback
  - stanza: iface
    target: {{ env.iface }}
    family: inet
    method: manual
    contents: []
  - stanza: iface
    target: br-main
    family: inet
    method: manual
    # In production, we have static ip. In other
    # environments, we use DHCP.
    contents:
      - pre-up     ip link add name br-main type bridge
      - pre-up     ip link set dev {{ env.iface }} master br-main
{% if 'prd' != env.name %}
      - up         dhclient br-main
      - down       dhclient -r br-main
{% else %}
      - pre-up     ip addr add 132.208.132.156/24 dev br-main
      - up         ip link set dev br-main up
      - up         ip route add default via br-main
      - down       ip link set dev br-main down
{% endif %}
      - post-down  ip link set dev {{ env.iface }} nomaster
      - post-down ip link del dev br-main
  - stanza: iface
    target: br-inf
    family: inet
    method: manual
    contents:
      - pre-up ip link add name br-inf type bridge
      - pre-up ip addr add 169.254.0.254/24 dev br-inf
      - up     ip link set dev br-inf up
      - down   ip link set dev br-inf down
      - post-down ip addr del dev 169.254.0.254/24 dev br-inf
      - post-down ip link del dev br-inf
  - stanza: iface
    target: br-srv
    family: inet
    method: manual
    contents:
      - pre-up ip link add name br-srv type bridge
      - up     ip link set dev br-srv up
      - down   ip link set dev br-srv down
      - post-down ip link del dev br-srv
  - stanza: iface
    target: br-dev
    family: inet
    method: manual
    contents:
      - pre-up ip link add name br-dev type bridge
      - up     ip link set dev br-dev up
      - down   ip link set dev br-dev down
      - post-down ip link del dev br-dev
  - stanza: iface
    target: br-pub
    family: inet
    method: manual
    contents:
      - pre-up ip link add name br-pub type bridge
      - up     ip link set dev br-pub up
      - down   ip link set dev br-pub down
      - post-down ip link del dev br-pub
  - stanza: iface
    target: br-ctf
    family: inet
    method: manual
    contents:
      - pre-up ip link add name br-ctf type bridge
      - up     ip link set dev br-ctf up
      - down   ip link set dev br-ctf down
      - post-down ip link del dev br-ctf
