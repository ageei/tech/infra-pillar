
This directory contains LXD profile; a default one and one for each
container. The default profile simply puts low resource limits and
restricts the environment to a maximum. Each container is then
configured individually with their own profile.
