
telegraf:
  config: |
    [[inputs.elasticsearch]]
      servers = [ "http://127.0.0.1:9200" ]
    [[inputs.kibana]]
      servers = [ "http://127.0.0.1:5601" ]
    [[inputs.nginx]]
      urls = [ "http://127.0.0.1/status" ]
