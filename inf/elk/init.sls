
include:
  - inf.elk.apt
  - inf.elk.logstash
  - inf.elk.nginx
  - inf.elk.rules
  - inf.elk.telegraf
