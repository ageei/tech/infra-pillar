#!jinja|yaml

{% import "env.jinja" as env %}


rules.v4: |
{%- if 'final' == env.stage %}
 *filter
 :INPUT   DROP [0:0]
 :FORWARD DROP [0:0]
 :OUTPUT  DROP [0:0]
 -A INPUT  -i lo -j ACCEPT
 -A OUTPUT -o lo -j ACCEPT
 -A INPUT  -m state --state ESTABLISHED,RELATED -j ACCEPT
 -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
 -A INPUT  -i eth0 -p tcp --dport 80    -j ACCEPT -m comment --comment "incoming http"
 -A INPUT  -i eth0 -p tcp --dport 1514  -j ACCEPT -m comment --comment "incoming syslog"
 -A OUTPUT -o eth0 -p udp --dport 53        -d 9.9.9.9/32 -j ACCEPT -m comment --comment "outgoing dns"
 -A OUTPUT -o eth0 -p tcp --dport 80        -d 169.254.0.2/32 -j ACCEPT -m comment --comment "outgoing pkg install"
 -A OUTPUT -o eth0 -p tcp --dport 8086      -d 169.254.0.4/32 -j ACCEPT -m comment --comment "outgoing metrics"
 -A OUTPUT -o eth0 -p tcp --dport 4505:4506 -d 169.254.0.5/32 -j ACCEPT -m comment --comment "outgoing salt"

 -A INPUT   -j NFLOG
 -A FORWARD -j NFLOG
 -A OUTPUT  -j NFLOG
 COMMIT
{% endif %}
