
logstash:
  use_upstream_repo: False

  inputs:
    -
      plugin_name: relp
      port: 1514
      type: rsyslog

  filters:
    -
      plugin_name: grok
      pattern_definitions:
          '"_IP"': "(?:%{IP}|\\[[^]]+\\])"
      match:
          message: "%{_IP:srcaddr}:%{POSINT:srcport} -> %{_IP:bindaddr}:%{POSINT:bindport} -> %{_IP:dstaddr}:%{POSINT:dstport} \\[%{HOSTNAME:hostname}\\] %{INT:o1}/%{INT:o2} bytes tx %{POSINT:i1}/%{POSINT:i2} bytes rx %{NUMBER:response_time} seconds"

  outputs:
    -
      plugin_name: elasticsearch
      hosts:
        - '127.0.0.1:9200'
