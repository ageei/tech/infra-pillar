#!jinja|yaml

groups:
  pg:
    state: present
    gid: 10000
  charles:
    state: present
    gid: 10001

users:
  pg:
    state: present
    uid: 10000
    gid: 10000
    system: False
    ssh_auth:
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCc5MYEH+u2jrpt7J9KepRHR9rhdyYaAhgCQXk1hCla9u55pBP6iJoQexrfF5J74K/z1IGh0MiJFZ/LcasHuoa5gxqo6aYfW3PyJUt25MPuEOXItnejU4mpos1qOUoEuCzM2wXvQwjaWvuNymtYeYofUK+1WgA8a85ym2blKwjyqU+uxRN5xEtX1Q8s/ft0+3bHqxnKqgwUyqJRTvoR1hIJfcZvsBltGDBEpO6X4EpIyF/iv2F1t/9uYqQeBHir6XjdL6Tf6onsddTsh/Ii3JBRxaVQd66f/GwoFYK+xy71vmMOnaNGOydd3nydFlJepZPk0NZhuXZK7HVUVV9w4/jWJp7KJwvx9EZX0j5A7fDhdXHb70aGdY1NPVBBq5aoKJAXTu4Aec7UdDE/vqnKGkT0WBo7xjcyheucFkMGFDyIEsMlaIv0hYAxY0GGD/5XlyHAPtVKtSxr7gNGxWIm4My64uzK3NXTt7EQtkODyEgBLeHBQvxzJQypJtdxIvR4A12x4AD5jsesjDoawAOXoKCJyT819q1fAMAVzm7YXa4ooXKHFU4HU33s4TdyW9Cx0ugxDqo2yxCIpnWhMWTdA54HOUsi/2ootEUzYnS0eKsTfA7Vjw8H8uqUy+OSxNBqaJZuHgRHD0mHTVm1xxEfw1Y5aNiE6SQeoxJGlomDEVYYtw==
    groups:
      - sudo

  charles:
    state: present
    uid: 10001
    gid: 10001
    system: False
    ssh_auth:
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDj+T37IcGTrPuFkIHwa+me+1GQUIRA/eQnCrpbV4AVhtggpUwiOaSMY7sKAHMgzPnFRRWCqUdgV+pmlHPq0HVyaFHjRpEQAZrCrqKAVyMOiX//AvG5yc1lGROR1hryWDIGrXjkabkGOLiRhMux3iCehcoYWqPb8u5FOtsIZA1DBEfu8sDjflGFOOxYN09j3qBZjZ5ZdUQOYWqbVKdgT8giA/vi0OuifE37eLRMN/Djc+2G6Dcyw31KpR2M0HZqZ+hG73u27VsAHBkKSVYaZhnFEcHfmLIpU12bKdoekwQxTN47XusTSvq2Hzz3DU2P28V05HwdldCUdoZqDxzi65lC7F2TQxM6pd/v5mcxgbiTPX7Vk5Ft91epMeeEObAqyfzUcJJCcm8VAmbu7m6yKDerjlf1K7+n4YNvQYDxgs6DdosDe1LEml0S2XqBHiboTOnpPWJoIGDT1K01S4TdLfIKdNeRlu0/z8eLPB8gpJ+I80zNl3gJhibFUy92ugJX8Fm0JlZWdwq/sVX915AIMU6i6rwbC4+G70mrxfvTQbLKJGSwJa0QPgY3IQVJMxq3tRqoeh5aICGKfheJdx8blEqzqJierC7YYwfEp7pkRjtk7VhrPEJolQQWsn8uRdpQbb2udIlo4TLALn/Qex6HSAcUQ1zK+lDPw2V2Bd5MVNcTuw==
    groups:
      - sudo
