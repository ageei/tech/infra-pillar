
nginx:
  install_from_repo: False
  install_from_phusionpassenger: False
  install_from_ppa: False

  lookup:
    package: nginx-light

  service:
    enable: True

  servers:
    managed:
      default:
        enabled: True
        config:
          - server:
            - server_name: '_'
            - listen:
              - '80 default_server'
            - 'location = /status':
              # WARNING This is a hack. nginx ignores the 1 and
              # it keeps nginx-formula from putting stub_status on the
              # same line as the location -\_(O.O)_/-
              - stub_status: 1
              - allow: 127.0.0.1
              - deny: all
            - 'location /':
              - proxy_pass: 'http://127.0.0.1:3000'
              - proxy_http_version: '1.1'
              - proxy_set_header: 'Upgrade $http_upgrade'
              - proxy_set_header: "Connection 'upgrade'"
              - proxy_set_header: 'Host $host'
              - proxy_cache_bypass: '$http_upgrade'
