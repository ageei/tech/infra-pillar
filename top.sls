base:
  '*': [ 'default' ]

  'inf-geo':   [ 'inf.geo' ]
  'inf-gw':    [ 'inf.gw' ]
  'inf-apt':   [ 'inf.apt' ]
  'inf-elk':   [ 'inf.elk' ]
  'inf-tig':   [ 'inf.tig' ]
  'inf-salt':  [ 'inf.salt' ]
  'dev-lxd':   [ 'dev.lxd' ]
  'pub-wiki':  [ 'pub.wiki' ]
  'ctf-ctfd':  [ 'ctf.ctfd' ]
  'ctf-web':   [ 'ctf.web' ]
  'ctf-pwn':   [ 'ctf.pwn' ]
