
postgres:
  use_upstream_repo: False

  acls:
    - ['local',   'all', 'telegraf', 'peer']
    - ['local',   'all', 'ctfd',     'peer']

  users:
    ctfd:
      ensure: present
      createdb: False
      createroles: False
      encrypted: True
      login: True
      replication: False
    telegraf:
      ensure: present
      createdb: False
      createroles: False
      login: True

  databases:
    ctfd:
      owner: ctfd
