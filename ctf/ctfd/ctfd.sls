#!jinja|yaml

{% import "ctf/ctfd/secrets.jinja" as ctfd %}


ctfd: |
  REDIS_URL=redis://127.0.0.1:6379
  MAILFROM_ADDR=ctf@ageei.org
  MAIL_USEAUTH=true
  MAIL_USERNAME=ctf@ageei.org
  MAIL_PASSWORD='{{ ctfd.mailpass }}'
  MAIL_TLS=true
  LOG_FOLDER=/var/log/ctfd
  UPLOAD_FOLDER=/var/cache/ctfd
  SECRET_KEY='{{ ctfd.secretkey }}'
  DATABASE_URL='postgres://ctfd@/ctfd?host=/var/run/postgresql'
  # export variables to facilitate debugging
  export DATABASE_URL SECRET_KEY UPLOAD_FOLDER LOG_FOLDER REDIS_URL
  export MAILFROM_ADDR MAIL_USEAUTH MAIL_USERNAME MAIL_PASSWORD MAIL_TLS
