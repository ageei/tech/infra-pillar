#!jinja|yaml

{% import "env.jinja" as env %}


rc.local:
  nginx: |
    dir='/etc/letsencrypt/live/ctf.{{ env.domains[0] }}'
    if [ ! -d "${dir}" ]; then
        mkdir -p "${dir}"
        openssl req -x509 -newkey rsa:2048 -keyout "${dir}/privkey.pem" -out "${dir}/fullchain.pem" -days 365 -nodes -subj "/CN=AGEEI snakeoil"
        /etc/init.d/nginx restart
    fi
