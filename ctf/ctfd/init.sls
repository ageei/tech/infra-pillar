#!jinja|yaml

{% import "env.jinja" as env %}


include:
  - ctf.ctfd.ctfd
  - ctf.ctfd.nginx
  - ctf.ctfd.pgsql
  - ctf.ctfd.rc-local
  - ctf.ctfd.rules
  - ctf.ctfd.telegraf

# Generate a stub X.509 certificate for image creation and deployment.
snakeoil:
  ctf.{{ env.domains[0] }}:
