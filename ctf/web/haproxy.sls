
# Simply drop an empty configuration. The file's content is managed
# by the project's own CI system.

chals:
  haproxy: |
    global
        log /dev/log    local0
        log /dev/log    local1 notice
        user haproxy
        group haproxy
        chroot  /var/lib/haproxy
        daemon
        stats socket /var/lib/haproxy/stats mode 660 level admin

    defaults
        log global
        mode http
        retries 3
        option      httplog
        option      dontlognull
        option      http-server-close
        option      forwardfor except 127.0.0.1
        option      redispatch
        maxconn 3000
        timeout http-request        10s
        timeout queue               1m
        timeout connect             10s
        timeout client              1m
        timeout server              1m
        timeout http-keep-alive     10s
        timeout check               10s

    frontend ft-http
        bind *:80
        mode http
