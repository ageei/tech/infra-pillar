#!jinja|yaml

# The docker-compose.yml file is managed by the project's own CI system.
# We just create one with a stub service to keep systemd, saltstack and
# the tests suite happy.

chals:
  docker-compose: |
      version: '2.2'
      services:
        nginx:
          image: nginx:alpine
          ports:
            - 10001:80
