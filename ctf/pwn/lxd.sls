#
# TODO adjust sub*id range with host, lxd preseed
#

lxd:
  lxd:
    run_init: True

  profiles:
    local:
      default:
        config:
          limits.cpu: 1
          limits.cpu.priority: 1
          limits.memory: 128MiB
          limits.processes: 256
          security.nesting: "true"
          security.privileged: "true"
        devices:
          root:
            type: disk
            path: /
            pool: default
            size: 2GiB
            limits.read: 20Iops
            limits.write: 10Iops
          # Nope, no networking with the outside.
