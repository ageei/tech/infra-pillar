#
# TODO adjust sub*id range with host, lxd preseed
#

lxd:
  lxd:
    run_init: True

    #init:
    #  storage_backend: TODO
    #  storage_pool: TODO

  profiles:
    local:
      default:
        config:
          limits.cpu: 1
          limits.cpu.priority: 1
          limits.memory: 1GiB
          limits.processes: 1024
          security.nesting: "true"
          security.privileged: "false"
        devices:
          root:
            type: disk
            path: /
            pool: default
            #size: 4GiB
            limits.read: 20Iops
            limits.write: 10Iops
          eth0:
            name: eth0
            type: nic
            nictype: bridged
            parent: lxdbr0
            security.mac_filtering: "true"

  images:
    local:
      ubuntu_18_04_amd64:
        name: 'ubuntu/18.04'
        source:
          name: bionic
          remote: ubuntu
        public: False
        auto_update: True
