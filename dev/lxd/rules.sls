#!jinja|yaml

{% import "env.jinja" as env %}


rules.v4: |
{%- if 'final' == env.stage %}
 *filter
 :INPUT   DROP [0:0]
 :FORWARD DROP [0:0]
 :OUTPUT  DROP [0:0]
 -A INPUT   -i lo -j ACCEPT
 -A OUTPUT  -o lo -j ACCEPT
 -A INPUT   -m state --state ESTABLISHED,RELATED -j ACCEPT
 -A OUTPUT  -m state --state ESTABLISHED,RELATED -j ACCEPT
 -A OUTPUT  -o eth0 -p udp --dport 53        -d 9.9.9.9/32 -j ACCEPT -m comment --comment "outgoing dns"
 -A OUTPUT  -o eth0 -p tcp --dport 80        -d 169.254.0.2/32 -j ACCEPT -m comment --comment "outgoing pkg install"
 -A OUTPUT  -o eth0 -p tcp --dport 1514      -d 169.254.0.3/32 -j ACCEPT -m comment --comment "outgoing syslog"
 -A OUTPUT  -o eth0 -p tcp --dport 8086      -d 169.254.0.4/32 -j ACCEPT -m comment --comment "outgoing metrics"
 -A OUTPUT  -o eth0 -p tcp --dport 4505:4506 -d 169.254.0.5/32 -j ACCEPT -m comment --comment "outgoing salt"

 # pulling container images (explicit drop internal, then accept 443)
 -A OUTPUT  -o eth0 -d 10/8       -j DROP -m comment --comment "drop to internal"
 -A OUTPUT  -o eth0 -d 169.254/16 -j DROP -m comment --comment "drop to internal .inf"
 -A OUTPUT  -o eth0 -p tcp --dport 443 -j ACCEPT -m comment --comment "https to internet"

 -A FORWARD -i lxdbr0 -d 10/8       -j DROP -m comment --comment "ci drop to internal"
 -A FORWARD -i lxdbr0 -d 169.254/16 -j DROP -m comment --comment "ci drop to internal .inf"

 # We may want to uncomment those in the future, if needed by developers.
 #-A FORWARD -i lxdbr0 -o eth0 -p tcp --dport 22   -j ACCEPT -m comment --comment "lxd container ssh"
 #-A FORWARD -i lxdbr0 -o eth0 -p tcp --dport 80   -j ACCEPT -m comment --comment "lxd container http"
 -A FORWARD -i lxdbr0 -o eth0 -p tcp --dport 443  -j ACCEPT -m comment --comment "lxd container https"
 -A FORWARD -o lxdbr0 -m state --state ESTABLISHED,RELATED -j ACCEPT

 -A INPUT   -j NFLOG
 -A FORWARD -j NFLOG
 -A OUTPUT  -j NFLOG
 COMMIT
{% endif %}
